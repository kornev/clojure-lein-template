(defproject empty "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :profiles {:dev {:dependencies [[org.clojure/tools.trace "0.7.8"]
                                  [pjstadig/humane-test-output "0.6.0"]]
                   :injections [(require 'pjstadig.humane-test-output)
                                (pjstadig.humane-test-output/activate!)]}
             :repl {:plugins [[cider/cider-nrepl "0.9.0-SNAPSHOT"]]}
             :uberjar {:aot :all
                       :omit-source true,
                       :manifest {"Built-By" "Vadim Kornev"}
                       :uberjar-exclusions [#"META-INF\/maven"
                                            #"META-INF\/leiningen"]}}
  :main ^:skip-aot empty.core
  :target-path "target/%s")

